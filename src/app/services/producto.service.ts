import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  constructor(private http:HttpClient) { }

  getProducto(url: string, page: number, size: number, order: string, asc: boolean):Observable<any>{
    return this.http.get<any>(`${url}/productos/buscar?` + `page=${page}&size=${size}&order=${order}&asc=${asc}`)
  }

  getMaestro(url: string, prefijo:number):Observable<any>{
    return this.http.get<any>(`${url}/maestros?prefijo=`+prefijo)
  }
  agregarProducto(url: string, producto: Object):Observable<any>{
    return this.http.post<any>(`${url}/productos`,producto)
  }
  getProductoById(url: string, id: number):Observable<any>{
    return this.http.get<any>(`${url}/productos/`+id)
  }
  editarProducto(url: string, producto: any):Observable<any>{
    return this.http.put<any>(`${url}/productos/`+producto['id'],producto)
  }
  eliminarProducto(url: string, id: number):Observable<any>{
    return this.http.delete<any>(`${url}/productos/`+id)
  }
  bajaProducto(url: string, id: number):Observable<any>{
    return this.http.patch<any>(`${url}/productos/`+id,{"estado":9})
  }
}
