import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private http:HttpClient) { }

  getCliente(url: string, page: number, size: number, order: string, asc: boolean):Observable<any>{
    return this.http.get<any>(`${url}/clientes/buscar?` + `page=${page}&size=${size}&order=${order}&asc=${asc}`)
  }
  getMaestro(url: string, prefijo:number):Observable<any>{
    return this.http.get<any>(`${url}/maestros?prefijo=`+prefijo)
  }
  agregarCliente(url: string, cliente: Object):Observable<any>{
    return this.http.post<any>(`${url}/clientes`,cliente)
  }
  getClienteById(url: string, id: number):Observable<any>{
    return this.http.get<any>(`${url}/clientes/`+id)
  }
  editarCliente(url: string, cliente: any):Observable<any>{
    return this.http.put<any>(`${url}/clientes/`+cliente['id'],cliente)
  }
  eliminarCliente(url: string, id: number):Observable<any>{
    return this.http.delete<any>(`${url}/clientes/`+id)
  }
  bajaCliente(url: string, id: number):Observable<any>{
    return this.http.patch<any>(`${url}/clientes/`+id,{"estado":9})
  }
}