export interface Cliente {
    id: number
    apellido: string
    nombre: string
    tipoPersona: {descripcion:string}
    tipoDocumento: {descripcion:string}
    numDocumento: string
    direccion: string
    referencia: string
    correo: string
    celular: string
    estado?: number
    idTipoPersona: number
    idTipoDocumento: number
    size?:number
}