export interface Producto {
    id: number
    idCategoria: number
    nombre: string
    precio: number
    stock: number
    estado?: number
    categoria: {descripcion:string}
}