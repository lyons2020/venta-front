import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  loading = false;

  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar, private router:Router) {
    this.form = this.fb.group({
      usuario: ['', Validators.required],
      clave: ['', Validators.required],
    })
  }

  ngOnInit(): void {
  }

  ingresar(){
    const usuario = this.form.value.usuario;
    const clave = this.form.value.clave;

    if(usuario == 'luis' && clave == '123'){
      // Redireccionar al dashboard
      this.fakeLoading()
    }else{
      // Mostramos mensaje de error
      this.error()
      this.form.reset()
    }
  }

  error(){
    this._snackBar.open(
      'Usuario o contraseña ingresados son inválidos',
      '',
      {
        duration: 5000,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      }
    )
  }

  fakeLoading(){
    this.loading = true
    setTimeout(() => {
      // Redireccionar al dashboard
      this.router.navigate(['dashboard'])
    },1500)
  }
}
