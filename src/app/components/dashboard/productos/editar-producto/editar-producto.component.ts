import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Maestro } from 'src/app/interfaces/maestro';
import { Producto } from 'src/app/interfaces/producto';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-editar-producto',
  templateUrl: './editar-producto.component.html',
  styleUrls: ['./editar-producto.component.css']
})
export class EditarProductoComponent implements OnInit {
  urlContext = '/lyons-api/v1'
  categoria: Maestro[] = []
  producto!: Producto
  form: FormGroup
  constructor(private fb: FormBuilder, 
              private _productoService:ProductoService,
              private router: Router, 
              private _snackBar:MatSnackBar,
              private route: ActivatedRoute
            ) { 
    this.form = this.fb.group({
      categoria: ['',Validators.required],
      nombre: ['',Validators.required],
      precio: ['',Validators.required],
      stock: ['',Validators.required],
    })
  }

  ngOnInit(): void {
    this.cargarCombos()
    this.cargarProducto()
  }

  cargarCombos(){
    this._productoService.getMaestro(this.urlContext,3).subscribe(res=>{
      this.categoria=res.data
    })
}

cargarProducto(){
  this.route.params.subscribe(params => {
    let id = params['id']
    this._productoService.getProductoById(this.urlContext,id).subscribe(res=>{
      this.form = this.fb.group(res.data)
    })
  })
}
editarProducto(){
  const producto = {
    id: this.form.value.id,
    idCategoria: this.form.value.categoria,
    nombre: this.form.value.nombre,
    precio: this.form.value.precio,
    stock: this.form.value.stock,
  }

  this._productoService.editarProducto(this.urlContext,producto).subscribe(res=>{
    this.router.navigate(['/dashboard/productos'])
  })
  
  this._snackBar.open(
    'Producto actualizado correctamente',
    '',
    {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom'
    }
  )
}

}
