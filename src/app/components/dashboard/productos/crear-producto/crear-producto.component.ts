import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Maestro } from 'src/app/interfaces/maestro';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})
export class CrearProductoComponent implements OnInit {
  urlContext = '/lyons-api/v1'
  categoria: Maestro[] = []
  form: FormGroup
  constructor(private fb: FormBuilder, 
              private _productoService:ProductoService,
              private router: Router, 
              private _snackBar:MatSnackBar
            ) { 
    this.form = this.fb.group({
      categoria: ['',Validators.required],
      nombre: ['',Validators.required],
      precio: ['',Validators.required],
      stock: ['',Validators.required],
    })
  }

  ngOnInit(): void {
    this.cargarCombos()
  }

  cargarCombos(){
    this._productoService.getMaestro(this.urlContext,3).subscribe(res=>{
      this.categoria=res.data
    })
  }
  agregarProducto(){
    const producto = {
      idCategoria: this.form.value.categoria,
      nombre: this.form.value.nombre,
      precio: this.form.value.precio,
      stock: this.form.value.stock,
    }
 
    this._productoService.agregarProducto(this.urlContext,producto).subscribe(res=>{
      this.router.navigate(['/dashboard/productos'])
    })
    
    this._snackBar.open(
      'Producto grabado correctamente',
      '',
      {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      }
    )
  }

}
