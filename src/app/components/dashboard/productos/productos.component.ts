import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Producto } from 'src/app/interfaces/producto';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  constructor(private _productoService: ProductoService, private _snackBar:MatSnackBar,private router:Router) { }
  urlContext = '/lyons-api/v1'
  listProductos: Producto[]=[]
  totalPages: Array<number>=[]
  page:  number =0;
  size: number = 5;
  order: string = "";
  asc: boolean= false;
  isFirst = false;
  isLast = false;

  ngOnInit(): void {
    this.cargarProductos()
  }

  rewind(): void {
    if (!this.isFirst) {
      this.page--;
      this.cargarProductos();
    }
  }

  forward(): void {
    if (!this.isLast) {
      this.page++;
      this.cargarProductos();
    }
  }

  setPage(page: number): void {
    this.page = page;
    this.cargarProductos();
  }

  cargarProductos(){
      this._productoService.getProducto(this.urlContext,this.page, this.size, this.order, this.asc).subscribe(res=>{
      this.listProductos=res.data.content
      this.isFirst = res.data.first;
      this.isLast = res.data.last;
      this.totalPages = new Array(res.data.totalPages);
    })
  }

  eliminar(id: number){
    this._productoService.eliminarProducto(this.urlContext,id).subscribe(res=>{
      this._snackBar.open(
        'Producto eliminado correctamente',
        '',
        {
          duration: 1500,
          horizontalPosition: 'center',
          verticalPosition: 'bottom'
        }
      )
      this.cargarProductos()
    })
  }
  baja(id: number){
    this._productoService.bajaProducto(this.urlContext,id).subscribe(res=>{})
  }

}
