import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from 'src/app/interfaces/cliente';
import { Maestro } from 'src/app/interfaces/maestro';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent implements OnInit {
  urlContext = '/lyons-api/v1'
  tipoPersona: Maestro[] = []
  tipoDocumento: Maestro[] = []
  cliente!: Cliente
  form: FormGroup
  constructor(private fb: FormBuilder, 
              private _clienteService:ClienteService,
              private router: Router, 
              private _snackBar:MatSnackBar,
              private route: ActivatedRoute
            ) { 
    this.form = this.fb.group({
      apellido: ['',Validators.required],
      nombre: ['',Validators.required],
      tipoPersona: ['',Validators.required],
      tipoDocumento: ['',Validators.required],
      numDocumento: ['',Validators.required],
      direccion: ['',Validators.required],
      referencia: [''],
      correo: [''],
      celular: [''],
    })
  }

  ngOnInit(): void {
    this.cargarCombos()
    this.cargarCliente()
  }

  cargarCombos(){
    this._clienteService.getMaestro(this.urlContext,1).subscribe(res=>{
      this.tipoPersona=res.data
    })
    this._clienteService.getMaestro(this.urlContext,2).subscribe(res=>{
      this.tipoDocumento=res.data
    })
}

cargarCliente(){
  this.route.params.subscribe(params => {
    let id = params['id']
    this._clienteService.getClienteById(this.urlContext,id).subscribe(res=>{
      this.form = this.fb.group(res.data)
    })
  })
}
editarCliente(){
  const cliente = {
    id: this.form.value.id,
    apellido: this.form.value.apellido,
    nombre: this.form.value.nombre,
    idTipoPersona: this.form.value.tipoPersona,
    idTipoDocumento: this.form.value.tipoDocumento,
    numDocumento: this.form.value.numDocumento,
    direccion: this.form.value.direccion,
    referencia: this.form.value.referencia,
    correo: this.form.value.correo,
    celular: this.form.value.celular,
  }

  this._clienteService.editarCliente(this.urlContext,cliente).subscribe(res=>{
    this.router.navigate(['/dashboard/clientes'])
  })
  
  
  this._snackBar.open(
    'Cliente actualizado correctamente',
    '',
    {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom'
    }
  )
}

}
