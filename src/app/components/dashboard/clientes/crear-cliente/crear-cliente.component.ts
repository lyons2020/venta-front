import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Maestro } from 'src/app/interfaces/maestro';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-crear-cliente',
  templateUrl: './crear-cliente.component.html',
  styleUrls: ['./crear-cliente.component.css']
})
export class CrearClienteComponent implements OnInit {
  urlContext = '/lyons-api/v1'
  tipoPersona: Maestro[] = []
  tipoDocumento: Maestro[] = []
  form: FormGroup
  loading = false;
  constructor(private fb: FormBuilder, 
              private _clienteService:ClienteService,
              private router: Router, 
              private _snackBar:MatSnackBar
            ) { 
    this.form = this.fb.group({
      apellido: ['',Validators.required],
      nombre: ['',Validators.required],
      tipoPersona: ['',Validators.required],
      tipoDocumento: ['',Validators.required],
      numDocumento: ['',Validators.required],
      direccion: ['',Validators.required],
      referencia: [''],
      correo: [''],
      celular: [''],
    })
  }

  ngOnInit(): void {
    this.cargarCombos()
  }

  cargarCombos(){
    this._clienteService.getMaestro(this.urlContext,1).subscribe(res=>{
      this.tipoPersona=res.data
    })
    this._clienteService.getMaestro(this.urlContext,2).subscribe(res=>{
      this.tipoDocumento=res.data
    })
}
  agregarCliente(){
    const cliente = {
    apellido: this.form.value.apellido,
    nombre: this.form.value.nombre,
    idTipoPersona: this.form.value.tipoPersona,
    idTipoDocumento: this.form.value.tipoDocumento,
    numDocumento: this.form.value.numDocumento,
    direccion: this.form.value.direccion,
    referencia: this.form.value.referencia,
    correo: this.form.value.correo,
    celular: this.form.value.celular,
    }
 
    this._clienteService.agregarCliente(this.urlContext,cliente).subscribe(res=>{
      this.router.navigate(['/dashboard/clientes'])
    })
    
    
    this._snackBar.open(
      'Cliente grabado correctamente',
      '',
      {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      }
    )
  }
}
