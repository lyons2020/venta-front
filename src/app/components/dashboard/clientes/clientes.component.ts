import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Cliente } from 'src/app/interfaces/cliente';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  urlContext = '/lyons-api/v1'
  form: FormGroup
  constructor(private _clienteService: ClienteService, private _snackBar:MatSnackBar,private router:Router,private fb: FormBuilder) { 
    this.form = this.fb.group({
      apellido: ['',Validators.required],
      nombre: ['',Validators.required],
      tipoPersona: ['',Validators.required],
      tipoDocumento: ['',Validators.required],
      numDocumento: ['',Validators.required],
      direccion: ['',Validators.required],
      referencia: [''],
      correo: [''],
      celular: [''],
    })
  }

  listClientes: Cliente[]=[]
  totalPages: Array<number>=[]
  page:  number =0;
  size: number = 5;
  order: string = "id";
  asc: boolean= false;
  isFirst = false;
  isLast = false;

  ngOnInit(): void {
    this.cargarClientes()
  }

  rewind(): void {
    if (!this.isFirst) {
      this.page--;
      this.cargarClientes();
    }
  }

  forward(): void {
    if (!this.isLast) {
      this.page++;
      this.cargarClientes();
    }
  }

  setPage(page: number): void {
    this.page = page;
    this.cargarClientes();
  }

  cargarClientes(){
      this._clienteService.getCliente(this.urlContext,this.page, this.size, this.order, this.asc).subscribe(
        res=>{
          this.listClientes=res.data.content
          this.isFirst = res.data.first
          this.isLast = res.data.last
          this.totalPages = new Array(res.data.totalPages)
        },
        err => {
          console.log(err.error)
        }
      )
  }

  eliminar(id: number){
    this._clienteService.eliminarCliente(this.urlContext,id).subscribe(res=>{
      this._snackBar.open(
        'Cliente eliminado correctamente',
        '',
        {
          duration: 1500,
          horizontalPosition: 'center',
          verticalPosition: 'bottom'
        }
      )
      this.cargarClientes()
    })
  }
  baja(id: number){
    this._clienteService.bajaCliente(this.urlContext,id).subscribe(res=>{})
  }
  buscar(){
    const cliente = {
      apellido: this.form.value.apellido,
      nombre: this.form.value.nombre,
      idTipoPersona: this.form.value.tipoPersona,
      idTipoDocumento: this.form.value.tipoDocumento,
      numDocumento: this.form.value.numDocumento,
      direccion: this.form.value.direccion,
      referencia: this.form.value.referencia,
      correo: this.form.value.correo,
      celular: this.form.value.celular,
    }
    if(this.form.invalid){
      this._clienteService.agregarCliente(this.urlContext,cliente).subscribe(res=>{
      
        this.router.navigate(['/dashboard/clientes'])
      })
      this._snackBar.open(
        'Cliente grabado correctamente',
        '',
        {
          duration: 1500,
          horizontalPosition: 'center',
          verticalPosition: 'bottom'
        }
      )
    }
  }
}
