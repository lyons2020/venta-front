import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientesComponent } from './clientes/clientes.component';
import { CrearClienteComponent } from './clientes/crear-cliente/crear-cliente.component';
import { EditarClienteComponent } from './clientes/editar-cliente/editar-cliente.component';
import { DashboardComponent } from './dashboard.component';
import { InicioComponent } from './inicio/inicio.component';
import { CrearProductoComponent } from './productos/crear-producto/crear-producto.component';
import { EditarProductoComponent } from './productos/editar-producto/editar-producto.component';
import { ProductosComponent } from './productos/productos.component';
import { ReportesComponent } from './reportes/reportes.component';

const routes: Routes = [
  {path:'', component:DashboardComponent, children:[
    {path:'',component:InicioComponent},
    {path:'clientes',component:ClientesComponent},
    {path:'reportes',component:ReportesComponent},
    {path:'crear-cliente',component:CrearClienteComponent},
    {path:'editar-cliente/:id',component:EditarClienteComponent},
    {path:'productos',component:ProductosComponent},
    {path:'crear-producto',component:CrearProductoComponent},
    {path:'editar-producto/:id',component:EditarProductoComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
